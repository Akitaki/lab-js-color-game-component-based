/**
 * UI-agnostic game logic part of the color game. Usage:
 *
 * 1. Construct by `new Core()`.
 * 2. Use `setMode()` and `pickCandidate()` to manipulate.
 * 3. Use `addEventListener()` to listen to game events.
 */
export default class Core {
  constructor() {
    /** @type {string[]} */
    this.candidateColors = [];
    /** @type {boolean[]} */
    this.candidateVisibilities = [];
    this.correctColor = '';
    this.mode = 'hard';

    this.numCandidatesForMode = {
      easy: 3,
      hard: 6,
      night: 6,
    };
    this.countdown = null;
    this.countdownSecsLeft = 6;

    /** @type {OnCoreEvent} */
    this.on = (event, listener) => {
      this.callbacks[event] = listener;
    };

    /**
     * Callbacks defaults to empty function
     * @type {Proxy & Callbacks}
     */
    this.callbacks = new Proxy(
      {},
      {
        get: (target, name) => {
          return target.hasOwnProperty(name)
            ? target[name]
            : () => {
                console.warn(`unlistened event ${name}`);
              };
        },
      }
    );
  }

  /** @param {number} index */
  pickCandidate(index) {
    if (!this.candidateVisibilities[index]) {
      console.error(`Index ${index} shouldn't be picked`);
      return;
    }

    if (this.correctColor === this.candidateColors[index]) {
      this.callbacks.gameWin(this.correctColor);
      this.candidateVisibilities.fill(false);
      if (this.countdown) clearInterval(this.countdown);
    } else {
      this.candidateVisibilities[index] = false;
      this.callbacks.wrongGuess();
      this.callbacks.candidatesUpdate(
        this.candidateColors,
        this.candidateVisibilities,
        this.correctColor
      );
    }
  }

  setMode(mode) {
    let oldNum = this.numCandidates;
    this.mode = mode;
    let newNum = this.numCandidates;
    if (oldNum !== newNum) {
      this.callbacks.numCandidatesUpdate(newNum);
    }
    this.renewColors();
  }

  renewColors() {
    this.candidateColors = Array(this.numCandidates).fill().map(this.randColor);
    this.correctColor = this.candidateColors[
      Math.floor(Math.random() * this.candidateColors.length)
    ];
    this.candidateVisibilities = Array(this.numCandidates).fill(true);

    // Reset countdown state
    if (this.countdown) {
      clearInterval(this.countdown);
    }

    if (this.mode === 'night') {
      this.countdownSecsLeft = 6;
      this.onCountdown();
      this.countdown = setInterval(() => {
        this.onCountdown();
      }, 1000);
    } else {
      this.callbacks.countdown('');
    }

    this.callbacks.candidatesUpdate(
      this.candidateColors,
      this.candidateVisibilities,
      this.correctColor
    );
  }

  init() {
    this.renewColors();
  }

  get numCandidates() {
    return this.numCandidatesForMode[this.mode];
  }

  onCountdown() {
    this.countdownSecsLeft -= 1;
    if (this.countdownSecsLeft > 0) {
      this.callbacks.countdown(this.countdownSecsLeft);
    } else {
      clearInterval(this.countdown);
      this.candidateVisibilities.fill(false);
      this.callbacks.timeout(this.correctColor);
    }
  }

  randColor() {
    return Math.floor(Math.random() * 16777216)
      .toString(16)
      .padStart(6, '0');
  }

  /** @param {string} color */
  static hexToRgb(color) {
    let r = (parseInt(color, 16) & 0xff0000) >> 16;
    let g = (parseInt(color, 16) & 0x00ff00) >> 8;
    let b = parseInt(color, 16) & 0x0000ff;
    return [r, g, b];
  }

  /** @param {string} color */
  static hexToRgbString(color) {
    return 'rgb(' + this.hexToRgb(color).join(',') + ')';
  }
}

/**
 * @typedef {(
 *     candidateColors: string[],
 *     candidateVisibilities: boolean[],
 *     correctColor: string
 * ) => void} CandidateUpdateCallback
 * @typedef {(numOfCandidates: number) => void} NumCandidateUpdateCallback
 * @typedef {(correctColor: string) => void)} GameWinCallback
 * @typedef {(correctColor: string) => void} TimeoutCallback
 * @typedef {() => void} WrongGuessCallback
 * @typedef {(secsLeft: number|string) => void} CountdownCallback
 *
 * @typedef {Object} Callbacks
 * @property {CandidateUpdateCallback} candidatesUpdate
 * @property {NumCandidateUpdateCallback} numCandidatesUpdate
 * @property {GameWinCallback} gameWin
 * @property {TimeoutCallback} timeout
 * @property {WrongGuessCallback} wrongGuess
 * @property {CountdownCallback} countdown
 *
 * @typedef {(event: 'candidatesUpdate', listener: CandidateUpdateCallback)} CandidateUpdateEvent
 * @typedef {(event: 'numCandidatesUpdate', listener: NumCandidateUpdateCallback)} NumCandidateUpdateEvent
 * @typedef {(event: 'gameWin', listener: GameWinCallback)} GameWinEvent
 * @typedef {(event: 'timeout', listener: TimeoutCallback)} TimeoutEvent
 * @typedef {(event: 'wrongGuess', listener: WrongGuessCallback)} WrongGuessEvent
 * @typedef {(event: 'countdown', listener: CountdownCallback)} CountdownEvent
 * @typedef {(
 *     CandidateUpdateEvent & NumCandidateUpdateEvent & GameWinEvent &
 *     WrongGuessEvent & CountdownEvent & TimeoutEvent
 * )} OnCoreEvent
 */
