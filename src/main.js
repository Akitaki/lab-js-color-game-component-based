import Component from './component.js';
import Navbar from './navbar.js';
import Board from './board.js';
import Deck from './deck.js';
import Reset from './reset.js';
import Core from './core.js';

import './main.css';

export default class Main extends Component {
  static getRootClass() {
    return '.main';
  }

  constructor(root) {
    super(root);

    this.navbar = new Navbar(root.querySelector('.navbar'));
    this.deck = new Deck(root.querySelector('.deck'));
    this.board = new Board(root.querySelector('.board'));
    this.reset = new Reset(root.querySelector('.reset'));
    this.blink = new Blink(root.querySelector('.blinkdiv'));

    this.core = new Core();

    /* Events from core */
    this.core.on(
      'candidatesUpdate',
      (candidateColors, candidateVisibilities, correctColor) => {
        this.root.style.backgroundColor = '#232323';
        this.deck.update(candidateColors, candidateVisibilities);
        this.board.showColor(Core.hexToRgbString(correctColor));
      }
    );
    this.core.on('gameWin', this.showGameOver.bind(this, 'correct'));
    this.core.on('timeout', this.showGameOver.bind(this, 'timeout'));
    this.core.on('countdown', (secsLeft) => {
      this.board.showCountdown(secsLeft);
      if (secsLeft) this.blink.blink();
    });
    this.core.on('wrongGuess', () => this.board.showWrongMessage());
    this.core.on('numCandidatesUpdate', (numCandidates) => {
      if (numCandidates === 3) this.deck.hideSecondRow();
      else this.deck.showSecondRow();
    });

    /* Hotkeys */

    this.hotkeys = new Hotkeys();

    // Card hotkeys
    for (let index = 0; index < 6; index += 1) {
      this.hotkeys.on(index.toString(), () => {
        this.core.pickCandidate(index);
      });
    }

    // Reload hotkey
    this.hotkeys.on('r', this.resetGame.bind(this));

    // Gamemode hotkeys
    this.hotkeys.on('e', () => this.navbar.clickMode('easy'));
    this.hotkeys.on('h', () => this.navbar.clickMode('hard'));
    this.hotkeys.on('n', () => this.navbar.clickMode('night'));

    /* Events from the member components */
    this.navbar.on('modeChange', (_firer, mode) => {
      this.core.setMode(mode);
      this.board.showOriginal();
      this.reset.showNewColor();
    });
    this.reset.on('click', this.resetGame.bind(this));
    this.deck.on('click', (_firer, index) => {
      this.core.pickCandidate(index);
    });

    this.core.renewColors();
  }

  /** Renew candidate colors, reset board message and button text */
  resetGame() {
    this.core.renewColors();
    this.board.showOriginal();
    this.reset.showNewColor();
  }

  /** Set the UI to game over state.  This includes the background color, the
   * cards, the reset button, and the message display.
   *
   * @param {('correct'|'timeout')} status
   * @param {string} correctColor
   */
  showGameOver(status, correctColor) {
    this.root.style.backgroundColor = '#' + correctColor;
    this.deck.update(Array(6).fill('ffffff'), Array(6).fill(true));
    this.reset.showPlayAgain();
    if (status === 'correct') {
      this.board.showCorrectMessage();
    } else if (status === 'timeout') {
      this.board.showTimeoutMessage();
    }
  }
}

class Blink extends Component {
  static getRootClass() {
    return '.blinkdiv';
  }

  constructor(root) {
    super(root);
  }

  blink() {
    this.root.style.backgroundColor = 'rgba(255, 255, 255, 0.2)';
    setTimeout(() => {
      this.root.style.backgroundColor = 'transparent';
    }, 50);
  }
}

class Hotkeys {
  constructor() {
    /** @type {(key: string, callback: () => void) => void} */
    this.on = (key, callback) => {
      this.callbacks[key] = callback;
    };

    /**
     * Callbacks defaults to empty function
     * @type {Proxy & Object.<string, () => void>}
     */
    this.callbacks = new Proxy(
      {},
      {
        get: (target, name) => {
          return target.hasOwnProperty(name)
            ? target[name]
            : () => {
                console.warn(`unlistened event ${name}`);
              };
        },
      }
    );

    document.addEventListener('keydown', (e) => {
      this.callbacks[e.key]();
    });
  }
}

window.onload = function () {
  const body = document.querySelector('body');
  new Main(body);
};
